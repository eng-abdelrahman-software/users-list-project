import { Component, OnInit } from '@angular/core';
import { User } from '../classes/User';
import { UserService } from '../services/user.service';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  users: User[];
  page: number = 1;
  total: number = 8;
  pageSize: number = 8;

  constructor(
    private userService: UserService,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.getHeroes(1);
  }

  public pageChanged(pageNum: number): void {
    this.getHeroes(pageNum);
  }

  getHeroes(page : number): void {
    this.userService.getHeroes(page)
      .subscribe(data => {
          this.users = data.data;
          this.page = data.page;
          this.total = data.total;
          this.pageSize = data.per_page;
        }
      );
  }
}
