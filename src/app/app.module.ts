import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { HttpCacheInterceptorModule } from '@ngneat/cashew';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserSearchComponent } from './user-search/user-search.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {LoaderService} from "./services/loader.service";
import {LoaderInterceptor} from "./intercepters/loader.interceptor";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ProgressBarIndeterminateExample} from './components/progressBar/progress-bar-indeterminate-example';
import {MatProgressBarModule} from "@angular/material/progress-bar";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpCacheInterceptorModule.forRoot(),
    NgbModule,
    BrowserAnimationsModule,
    MatProgressBarModule
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    UserDetailComponent,
    UserSearchComponent,
    ProgressBarIndeterminateExample
  ],
  providers: [
    LoaderService,
    { provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    }
  ],
  bootstrap: [ AppComponent, ProgressBarIndeterminateExample ]
})
export class AppModule { }
