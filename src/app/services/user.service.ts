import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { GlobalObject } from '../classes/GlobalObject';
import { GlobalSimpleObject } from '../classes/GlobalSimpleObject';
import { withCache } from '@ngneat/cashew';

@Injectable({ providedIn: 'root' })
export class UserService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }

  getHeroes(page: number): Observable<GlobalObject> {
    const url = `https://reqres.in/api/users?page=${page}`;
    return this.http.get<GlobalObject>(url, {context: withCache()}).pipe();
  }

  getHero(id: number): Observable<GlobalSimpleObject> {
    const url = `https://reqres.in/api/users/${id}`;
    return this.http.get<GlobalSimpleObject>(url, {context: withCache()}).pipe();
  }

}
