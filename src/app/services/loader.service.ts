import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class LoaderService {
  constructor() {}
  isLoading = new Subject<boolean>();
  value : number;

  show() {
    this.isLoading.next(true);
  }

  hide() {
    setTimeout(()=>{
      this.isLoading.next(false);
    }, 500);
  }
}
