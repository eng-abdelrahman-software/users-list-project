import { Component, OnInit } from '@angular/core';

import { User } from '../classes/User';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-hero-search',
  templateUrl: './user-search.component.html',
  styleUrls: [ './user-search.component.css' ]
})
export class UserSearchComponent implements OnInit {
  user: User;

  constructor(private userService: UserService) {}

  search(id: string): void {
    this.userService.getHero(Number(id))
      .subscribe(user => {
        if(user){
          this.user = user.data;
        }
      });
  }

  ngOnInit(): void {}
}
