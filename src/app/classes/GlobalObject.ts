import { User } from './User';

export interface GlobalObject {
  data: User[];
  page: number;
  per_page: number;
  support: any;
  total: number;
  total_pages: number;
}
