
<p align="center"><a href="https://angular.io/" target="_blank"><img src="https://miro.medium.com/max/960/1*FZDQJ7ZMtUuIDVhQR0mXsw.png" width="400"></a></p>
<p align="center">
  <br>
  <i>Angular is a development platform for building mobile and desktop web applications
    <br> using Typescript/JavaScript and other languages.</i>
  <br>
</p>

<p align="center">
  <a href="https://www.angular.io"><strong>www.angular.io</strong></a>
  <br>
</p>

<p align="center">
  <a href="CONTRIBUTING.md">Contributing Guidelines</a>
  ·
  <a href="https://github.com/angular/angular/issues">Submit an Issue</a>
  ·
  <a href="https://blog.angular.io/">Blog</a>
  <br>
  <br>
</p>


<p align="center">
  <a href="https://circleci.com/gh/angular/workflows/angular/tree/master">
    <img src="https://img.shields.io/circleci/build/github/angular/angular/master.svg?logo=circleci&logoColor=fff&label=CircleCI" alt="CI status" />
  </a>&nbsp;
  <a href="https://www.npmjs.com/@angular/core">
    <img src="https://img.shields.io/npm/v/@angular/core.svg?logo=npm&logoColor=fff&label=NPM+package&color=limegreen" alt="Angular on npm" />
  </a>&nbsp;
  <a href="https://discord.gg/angular">
    <img src="https://img.shields.io/discord/463752820026376202.svg?logo=discord&logoColor=fff&label=Discord&color=7389d8" alt="Discord conversation" />
  </a>
</p>

<hr> 


Angular project about browsing users list : 

- users list
- user details
- back button
- immediat search by id
- pagination
- caching
- loading

## Installation 

open a new directory and write the following commends

```
git clone https://gitlab.com/abd.r.horani/users-list-maids.git
```

```
cd users-list-maids
```

```
npm install
```


```
ng serve
```


then you can browse the application : on 

[localhost:4200/](localhost:4200/)

 
 appriciate your time
   